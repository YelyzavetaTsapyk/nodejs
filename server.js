const express = require('express');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const app = express();
app.use(express.json());

const files = [
    {
        "filename": "notes.txt",
        "content": "Even Devil cried when did node js task",
        "uploadedDate": new Date(),
        "password": '$2b$10$LkE5GwtaazLsFyI4cSMdKeipqKEhlHtBQMyPXl6COREJ6vLjCZnSG'
    }, {
        "filename": "info.txt",
        "content": "NodeJS killed more people than sharks",
        "uploadedDate": new Date()
    }, {
        "filename": "music.txt",
        "content": "Music for your black soul",
        "uploadedDate": new Date()
    }
]

function createFile(req, res) {
    try {
        const file = {
            "filename": req.body.filename,
            "content": req.body.content,
            "uploadedDate": new Date()
        }
        const index = files.findIndex((file) =>
            file.filename === req.body.filename);
        if (index === -1) {
            if (req.body.password) {
                bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
                    file.password = hash;
                });
            }
            files.push(file);
            res.send({
                "message": "File created successfully"
            })
        } else {
            updateFile(req, res, file, index);
        }
    } catch (e) {
        res.status(400).send({
            "message": "Please specify 'content' parameter"
        })
    }
}

function updateFile(req, res, file, index) {
    if (req.body.password) {
        console.log('IF')
        bcrypt.compare(req.body.password, files[index].password, function (err, result) {
            if (result) {
                file.password = files[index].password;
                files[index] = file;
                files[index].uploadedDate = new Date();
                res.send({
                    "message": "File updated successfully"
                })
            } else {
                res.send({
                    "message": "Invalid passport"
                })
            }
        });
    } else {
        console.log('ELSE')
        files[index] = file;
        files[index].uploadedDate = new Date();
        res.send({
            "message": "File updated successfully"
        })
    }
}

function getLockedFile(req, res) {
    try {
        const file = files.find((file) =>
            file.filename === req.body.filename);
        if (file.password && req.body.password) {
            bcrypt.compare(req.body.password, file.password, function (err, result) {
                if (result) {
                    res.send({
                        "message": "Success",
                        "filename": file.filename,
                        "content": file.content,
                        "uploadedDate": file.uploadedDate,
                        "extension": file.filename.split('.')[file.filename.split('.').length - 1],
                    })
                } else {
                    res.status(400).send({
                        "message": `Passport is invalid`
                    })
                }
            });
        } else {
            res.redirect(`/api/files/${file.filename}`)
        }
    } catch (e) {
        res.status(400).send({
            "message": `Invalid input data`
        })
    }
}

app.get('/', (req, res) => {
    res.send('Hello, API')
})

app.get('/api/files', (req, res) => {
    try {
        res.send({"message": "Success", "files": files.map((file) => file.filename)})
    } catch (error) {
        res.status(400).send({
            "message": "Client error"
        })
    }
})

app.get('/api/files/:filename', (req, res) => {
    try {
        const file = files.find((file) =>
            file.filename === req.params.filename);
        if (file.password) {
            res.status(400).send({
                "message": `File is locked`
            })
        } else {
            res.send({
                "message": "Success",
                ...file,
                "extension": file.filename.split('.')[file.filename.split('.').length - 1],
            })
        }

    } catch (error) {
        res.status(400).send({
            "message": `No file with '${req.params.filename}' filename found`
        })
    }
})

app.post('/api/files', (req, res) => {
    try {
        if (req.body.content) {
            createFile(req, res);
        } else {
            getLockedFile(req, res);
        }
    } catch (error) {
        res.status(400).send({
            "message": "Please specify 'content' parameter"
        })
    }
})

app.listen(8080, () => {
    console.log('API app start')
})
